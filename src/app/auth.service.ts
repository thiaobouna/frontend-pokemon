import { Injectable } from '@angular/core';
import { Observable, delay, of, tap } from 'rxjs';
import { PokemonService } from './pokemon.service';
import { Pokemon } from './pokemon/pokemon';
import { users } from './pokemon/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn: boolean = false;
  redirectUrl: string;
  pok:users| undefined;

  constructor( private Pokemonsrvice: PokemonService ) {}

  login(name: string, password: string): Observable<boolean> {
 
 this.Pokemonsrvice.getuserByName(name).subscribe(poknew =>  {this.pok= poknew
 });

    const isLoggedIn = (name == 'bouna' && password == 'bouna');

    return of(isLoggedIn).pipe(
      delay(1000),
      tap(isLoggedIn => this.isLoggedIn = isLoggedIn)
    );

    
  }

  logout() {
    this.isLoggedIn = false;
  }
}
