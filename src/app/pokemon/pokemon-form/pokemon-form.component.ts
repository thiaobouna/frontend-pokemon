import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../pokemon';
import { PokemonService } from 'src/app/pokemon.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as e from 'express';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styles: [
  ]
})
export class PokemonFormComponent implements OnInit {

  @Input() pokemon: Pokemon;
  types: string[];
  isAddForm: boolean;
  pokemonId:any;
  updateForm: Pokemon;

  pokemonnew:Pokemon={
id:0,
name:'',
hp:0,
cp:0,
picture:'',
types:[],
created: new Date

  }
     
     
  constructor(
    private route:ActivatedRoute, 
    private pokemonService: PokemonService,
    private router: Router
  ) { }

  ngOnInit() {
    this.types = this.pokemonService.getPokemonTypeList();
    this.isAddForm = this.router.url.includes('add');
    const pokemonIds: string|null = this.route.snapshot.paramMap.get('id');
    this.pokemonId=pokemonIds;
    

    const json=this.pokemon.types;
  const jsons=json.toString();
    const ab=JSON.parse(jsons);
   const results=ab.toString();
   const result=results.split(",");
   this.pokemon.types=result;
}


  
  hasType(type: string): boolean {

  
  

    return this.pokemon.types.includes(type);
  }

  selectType($event: Event, type: string) {
    const isChecked: boolean = ($event.target as HTMLInputElement).checked;

    if(isChecked) {
      this.pokemon.types.push(type);
    } else {
      const index = this.pokemon.types.indexOf(type);
      this.pokemon.types.splice(index, 1);
    }
  }

  isTypesValid(type: string): boolean {
    if(this.pokemon.types.length == 1 && this.hasType(type)) {
      return false;
    }

    if(this.pokemon.types.length > 2 && !this.hasType(type)) {
      return false;
    }

    return true;
  }



  onSubmit() {

const data={
  name:this.pokemon.name,
  hp:this.pokemon.hp,
  cp:this.pokemon.cp,
  picture:this.pokemon.picture,
  types:JSON.stringify(this.pokemon.types),
  created:'2000-06-06'
}

const dataupdate={
  name:this.pokemon.name,
  hp:this.pokemon.hp,
  cp:this.pokemon.cp,
  picture:this.pokemon.picture,
  types:this.pokemon.types,
  created:'2000-06-06'
}

if(this.isAddForm) {
  this.pokemonService.addPokemon(data)
    .subscribe((pokemon: Pokemon) => this.router.navigate(['/pokemon', pokemon.id]));
} else {
  this.pokemonService.updatePokemon(this.pokemonId,data)
    .subscribe(() => this.router.navigate(['/pokemon', this.pokemon.id]));
}
  }


}
