import { Component } from '@angular/core';
import { Pokemon } from '../pokemon';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from 'src/app/pokemon.service';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styles: [
  ]
})
export class DetailPokemonComponent {


  pokemonList: Pokemon[];
  pokemon: Pokemon|undefined;
  pokemonIds:any;
  
  

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private pokemonService: PokemonService
  ) { }
  

  ngOnInit() {

    const pok=new Pokemon;
    const pokemonId: string|null = this.route.snapshot.paramMap.get('id');
    this.pokemonIds=pokemonId
    
    if(pokemonId) {
      this.pokemonService.getPokemonById(+pokemonId)
        .subscribe(pokemon => this.pokemon = pokemon);
    }
  }


  json2array(json:string[]){

    const jsons=json.toString();
    const ab=JSON.parse(jsons);
    const results=ab.toString();
    const result=results.split(",");

    let myob=new Pokemon;
    myob.types=result;
    
   return myob;

    
}

  deletePokemon(pokemon: Pokemon) {
    this.pokemonService.deletePokemonById(pokemon.id)
      .subscribe(() => this.goToPokemonList());
  }

  goToPokemonList() {
    this.router.navigate(['/pokemon']);
  }

  goToEditPokemon(pokemon: Pokemon) {
    this.router.navigate(['/edit/pokemon', pokemon.id]);
  }


}
