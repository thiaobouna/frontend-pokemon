import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <nav>
  <div class="nav-wrapper teal">
      <a href="#" class="brand-logo center">
          Pokédex
      </a>
  </div>
</nav>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  
}
