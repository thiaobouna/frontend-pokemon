import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of, tap } from 'rxjs';
import { Pokemon } from './pokemon/pokemon';
import { users } from './pokemon/user';

const baseurl='http://localhost:8080/api/pokemon';
const baseurluser='http://localhost:8080/api/user';
@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  
  constructor(private http: HttpClient) {}

  getPokemonList(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(baseurl).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, []))
    );
  }

  getPokemonById(pokemonId: number): Observable<Pokemon|undefined> {
    return this.http.get<Pokemon>(`${baseurl}/${pokemonId}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, undefined))
    );
  }

  getuserByName(userNam: string): Observable<users|undefined> {
   
    return this.http.get<Pokemon[]>(`${baseurluser}?username=${userNam}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, []))
    );

  }


  searchPokemonList(term: string): Observable<Pokemon[]> {
    if(term.length <= 1) {
      return of([]);
    } 

    return this.http.get<Pokemon[]>(`${baseurl}?name=${term}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, []))
    );
  }

  updatePokemon(id:any,data: any): Observable<null> {
    
   
    return this.http.put(`${baseurl}/${id}`, data).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    );
  }

  


  addPokemon(data: any): Observable<Pokemon> {
    return this.http.post<Pokemon>(baseurl, data).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    );
  }



  deletePokemonById(pokemonId: number): Observable<null> {
    return this.http.delete(`${baseurl}/${pokemonId}`).pipe(
      tap((response) => this.log(response)),
      catchError((error) => this.handleError(error, null))
    );
  }

  private log(response: any) {
    console.table(response);
  }

  private handleError(error: Error, errorValue: any) {
    console.error(error);
    return of(errorValue);
  }

  getPokemonTypeList(): string[] {
    return [
      'Plante', 
      'Feu', 
      'Eau', 
      'Insecte',
      'Normal',
      'Electrik', 
      'Poison', 
      'Fée',
      'Vol',
      'Combat',
      'Psy'
    ];
  }
}
